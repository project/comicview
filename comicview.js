var comicview_files = [];

function comicviews_add_file ( nid, filepath ) {
  var id = 'node-'+nid;
  if (!comicview_files[id]) comicview_files[id] = [];
  comicview_files[id].push(filepath);
}

$().ready(function() {

for (key in comicview_files) {
  
  var cv = $('#'+key+' .content');
  cv.prepend('<div class="comicview"><div class="comicview-display"></div><div class="comicview-pager"></div></div>');
  
  // mount pager
  var cvpager = $('#'+key+' .content .comicview .comicview-pager');
  $.each(comicview_files[key], function(i, val) {
    var pager = '<a href="javascript: void(0);" class="'+i+'">'+(i+1)+'</a>';
    cvpager.append(''+pager+'');
  });
  
  $('a', cvpager).click(function() {
    //var path = Drupal.settings.comicview.base_path + comicview_files[key][$(this).attr('class')];
    var path = comicview_files[key][$(this).attr('class')];
    var img = '<img src="'+path+'" />';
    
    $('.comicview .comicview-display', cv).html(img);
  });
  
  $('a:eq(0)', cvpager).trigger('click');
  
}

});
